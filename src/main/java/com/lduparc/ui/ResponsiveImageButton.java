package com.lduparc.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.widget.ImageButton;

import com.lduparc.ui.R;

public class ResponsiveImageButton extends ImageButton {

    private Context mContext;
    private Drawable mSelector;

    public ResponsiveImageButton(Context context) {
        super(context);
    }

    public ResponsiveImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initCustomProperties(context, attrs, 0);
    }

    public ResponsiveImageButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initCustomProperties(context, attrs, defStyle);
    }

    private void initCustomProperties(Context context, AttributeSet attrs, int defStyle) {
        mContext = context;

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.UIStyle);
        if (attributes != null && attributes.hasValue(R.styleable.UIStyle_uiselector)) {
            mSelector = attributes.getDrawable(R.styleable.UIStyle_uiselector);
            setSelectorDrawable(getDrawable());
        }
    }

    private void setSelectorDrawable(Drawable src) {
        if (src != null) {
            Drawable[] layers = new Drawable[2];
            layers[0] = src;
            layers[1] = mSelector;
            LayerDrawable layerDrawable = new LayerDrawable(layers);
            setImageDrawable(layerDrawable);
        }
    }

    @Override
    public void setImageResource(int resId) {
        setSelectorDrawable(mContext.getResources().getDrawable(resId));
    }
}
