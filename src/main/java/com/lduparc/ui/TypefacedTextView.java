package com.lduparc.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TypefacedTextView extends  TextView {

    public TypefacedTextView(Context context) {
        super(context);
    }

    public TypefacedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode())
            initCustomProperties(context, attrs, 0);
    }

    public TypefacedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if(!isInEditMode())
            initCustomProperties(context, attrs, defStyle);
    }

    private void initCustomProperties(Context context, AttributeSet attrs, int defStyle) {
        setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.UIStyle);
        String font = attributes.getString(R.styleable.UIStyle_uitypeface);
        Typeface typeface;
        if (attributes.getIndexCount() > 0 && font.length() > 0) typeface = Typefaces.get(context, font);
        else {
            int[] attrsArray = new int[] { android.R.attr.textStyle };
            TypedArray ta = context.obtainStyledAttributes(attrs, attrsArray);
            int textStyle = ta.getInt(0, -1);
            ta.recycle();
            typeface = textStyle == 1 ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT;
        }
        this.setTypeface(typeface, defStyle);
        attributes.recycle();
    }

}