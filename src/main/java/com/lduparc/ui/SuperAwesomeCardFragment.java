package com.lduparc.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;

public class SuperAwesomeCardFragment extends Fragment {

    private static final String ARG_LAYOUT = "layout";
    private static final String ARG_LAYOUT_TO_UPDATE = "layoutToUpdate";
    private static final String ARG_TEXT_TO_SET = "textToSet";

    private int layout;
    private int layoutToUpdate;
    private String textToSet;

    public static SuperAwesomeCardFragment newInstance(int res, final int layoutToUpdate, final String textToSet) {
        SuperAwesomeCardFragment f = new SuperAwesomeCardFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_LAYOUT, res);
        b.putInt(ARG_LAYOUT_TO_UPDATE, layoutToUpdate);
        b.putString(ARG_TEXT_TO_SET, textToSet);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        layout = getArguments().getInt(ARG_LAYOUT);
        layoutToUpdate = getArguments().getInt(ARG_LAYOUT_TO_UPDATE);
        textToSet = getArguments().getString(ARG_TEXT_TO_SET);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        FrameLayout fl = new FrameLayout(getActivity());
        fl.setLayoutParams(params);

        final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources()
                .getDisplayMetrics());

        params.setMargins(margin, margin, margin, margin);
        View v = LayoutInflater.from(getActivity()).inflate(layout, null);

        if (v != null) {
            if (layoutToUpdate != -1 && textToSet != null)
                ((TextView) v.findViewById(layoutToUpdate)).setText(textToSet);
            v.setLayoutParams(params);
            fl.addView(v);
        }
        return fl;
    }

}