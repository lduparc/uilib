package com.lduparc.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lduparc.ui.R;

public class FormFieldView extends LinearLayout {

    private EditText mEdit;

    public FormFieldView(Context context) {
        super(context);
    }

    public FormFieldView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }


    public FormFieldView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.form_field, this, true);

        TextView mName = (TextView) findViewById(R.id.fieldName);
        mEdit = (EditText) findViewById(R.id.fieldEdit);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.UIStyle);
        if (attributes != null) {
            String name = attributes.getString(R.styleable.UIStyle_uiname);
            String placeholder = attributes.getString(R.styleable.UIStyle_uiplaceholder);
            int maxlength = attributes.getInt(R.styleable.UIStyle_uimaxlength, 1000);
            if (name != null) mName.setText(name);
            if (placeholder != null) mEdit.setHint(placeholder);
            InputFilter[] fArray = new InputFilter[1];
            fArray[0] = new InputFilter.LengthFilter(maxlength);
            mEdit.setFilters(fArray);
            attributes.recycle();
        }
    }

    public void setError(String error) {
        if (mEdit != null) mEdit.setError(error);
    }

    public void setFieldInputType(int inputType) {
        if (mEdit != null) mEdit.setInputType(inputType);
    }

    public void setText(String name) {
        if (name != null && mEdit != null) {
            if (mEdit.getText() != null) mEdit.getText().clear();
            mEdit.append(name);
        }
    }

    public String getText() {
        if (mEdit != null && mEdit.getText() != null) return mEdit.getText().toString();
        return "";
    }

    public boolean isEmpty() {
        return mEdit == null || mEdit.length() == 0;
    }

    public EditText getEdit() {
        return mEdit;
    }
}
