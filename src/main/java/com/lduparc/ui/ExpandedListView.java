package com.lduparc.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.ViewGroup;

import com.lduparc.ui.jazzy.JazzyListView;

public class ExpandedListView extends JazzyListView {

    private int old_count = 0;

    public ExpandedListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        if (getCount() > 0) {
            final int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, expandSpec);
            final ViewGroup.LayoutParams params = this.getLayoutParams();
            if (params != null) {

                params.height = this.getMeasuredHeight();
                setLayoutParams(params);
            }
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

}