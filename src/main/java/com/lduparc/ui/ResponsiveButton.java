package com.lduparc.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;

import com.devspark.robototextview.widget.RobotoButton;
import com.lduparc.ui.R;


public class ResponsiveButton extends RobotoButton {

    private Drawable mSelector;

    public ResponsiveButton(Context context) {
        super(context);
    }

    public ResponsiveButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initCustomProperties(context, attrs, 0);
    }

    public ResponsiveButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initCustomProperties(context, attrs, defStyle);
    }

    private void initCustomProperties(Context context, AttributeSet attrs, int defStyle) {

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.UIStyle);
        if (attributes != null && attributes.hasValue(R.styleable.UIStyle_uiselector)) {
            mSelector = attributes.getDrawable(R.styleable.UIStyle_uiselector);
            setSelectorDrawable(getBackground());
        }
    }

    private void setSelectorDrawable(Drawable src) {
        Drawable[] layers = new Drawable[2];
        layers[0] = mSelector;
        layers[1] = mSelector;
        LayerDrawable layerDrawable = new LayerDrawable(layers);
        setBackgroundDrawable(layerDrawable);
    }

}
